package prediccion.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import prediccion.entities.CondicionClimatica;
import prediccion.entities.Estadistica;

/**
 * Created by leandro on 4/23/18.
 */
@Repository
public interface EstadisticaRepository extends JpaRepository<Estadistica, Long> {
}
