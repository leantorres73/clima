package prediccion.repositories;

import prediccion.entities.CondicionClimatica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by leandro on 4/23/18.
 */
@Repository
public interface CondicionClimaticaRepository extends JpaRepository<CondicionClimatica, Long> {
    CondicionClimatica findByDiaEquals(Integer dia);
}
