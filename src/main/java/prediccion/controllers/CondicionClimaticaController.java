package prediccion.controllers;

import prediccion.entities.CondicionClimatica;
import prediccion.services.CondicionClimaticaService;
import prediccion.services.exceptions.BusinessLogicException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static prediccion.ServicesApplication.CLIMA_URL;

/**
 * Created by leandro on 4/22/18.
 */
@RestController
@RequestMapping(CLIMA_URL)
public class CondicionClimaticaController {

    private static final Logger log = LoggerFactory.getLogger(CondicionClimaticaController.class);

    @Autowired
    CondicionClimaticaService condicionClimaticaService;

    /**
     * GET  /:dia: get the information of a specific day
     *
     * @return the ResponseEntity with
     * status 200 and body with the climatic condition information,
     */
    @ApiOperation(value = "Devuelve una condicion climatica para el dia ingresado")
    @ApiResponses(
        value = {
            @ApiResponse(code = 200, message = "Condicion climatica devuelta"),
            @ApiResponse(code = 404, message = "Condicion climatica no encontrada"),
            @ApiResponse(code = 400, message = "Día no válido")
        }
    )
    @GetMapping
    public ResponseEntity<CondicionClimatica> findCondicionClimaticaByDay(@RequestParam Integer dia) throws BusinessLogicException {
        log.debug("Fetching condicion climatica por dia");
        return new ResponseEntity<>(condicionClimaticaService.findCondicionClimaticaByDay(dia), HttpStatus.OK);
    }
}