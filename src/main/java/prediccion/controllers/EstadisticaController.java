package prediccion.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import prediccion.entities.Estadistica;
import prediccion.services.EstadisticaService;
import prediccion.services.exceptions.BusinessLogicException;

import static prediccion.ServicesApplication.ESTADISTICA_URL;

/**
 * Created by leandro on 4/22/18.
 */
@RestController
@RequestMapping(ESTADISTICA_URL)
public class EstadisticaController {

    private static final Logger log = LoggerFactory.getLogger(EstadisticaController.class);

    @Autowired
    EstadisticaService condicionClimaticaService;

    /**
     * GET
     *
     * @return the ResponseEntity with
     * status 200 and body with the statistics information,
     */
    @ApiOperation(value = "Devuelve las estadisticas del sistema")
    @ApiResponses(
        value = {
            @ApiResponse(code = 200, message = "Estadisticas del sistema"),
        }
    )
    @GetMapping
    public ResponseEntity<Estadistica> findEstadisticas() throws BusinessLogicException {
        log.debug("Fetching condicion climatica por dia");
        return new ResponseEntity<>(condicionClimaticaService.findEstadistica(), HttpStatus.OK);
    }
}