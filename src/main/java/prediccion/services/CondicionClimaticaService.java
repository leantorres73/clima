package prediccion.services;

import prediccion.entities.ClimaEnum;
import prediccion.entities.CondicionClimatica;
import prediccion.entities.Estadistica;
import prediccion.entities.PlanetaEnum;
import prediccion.repositories.CondicionClimaticaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import prediccion.services.exceptions.NotFoundException;
import prediccion.utils.TriangleUtils;

/**
 * Created by leandro on 4/23/18.
 */
@Service
@Transactional
public class CondicionClimaticaService {

    private static final Logger log = LoggerFactory.getLogger(CondicionClimaticaService.class);

    private CondicionClimaticaRepository condicionClimaticaRepository;

    private EstadisticaService estadisticaService;

    public CondicionClimaticaService(CondicionClimaticaRepository condicionClimaticaRepository,
                                     EstadisticaService estadisticaService) {
        this.condicionClimaticaRepository = condicionClimaticaRepository;
        this.estadisticaService = estadisticaService;
    }

    /**
     * Buscar condicion climatica por un dia
     * */
    @Transactional(readOnly = true)
    public CondicionClimatica findCondicionClimaticaByDay(Integer dia) throws NotFoundException {
        log.debug("findCondicionClimaticaByDay");
        CondicionClimatica condicionClimatica = condicionClimaticaRepository.findByDiaEquals(dia);
        if (condicionClimatica == null){
            throw new NotFoundException("dia no calculado");
        }
        return condicionClimatica;
    }

    /**
     * Metoodo que genera condiciones climaticas basado en una cantidad de años
     * */
    @Transactional
    public void generateClimaticConditions(Integer years) {
        log.debug("generate climatic conditions for {} years ", years);

        // periodo a calcular 365 dias por los años configurados
        int dias = 365 * years;
        // cuenta de periodos de sequia
        int sequiaCount = 0;
        // cuenta de periodos de lluvia
        int lluviaCount = 0;
        // cuenta de periodos de optimas condiciones
        int optimaCount = 0;
        // dia maximo de lluvia
        int maxDay = 0;
        // valor de area del dia maximo
        double maxArea = 0.0d;
        // ultimo clima registrado
        ClimaEnum lastWeather = null;

        // angulo real del prediccion Farengi para un dia especifico
        int posicionPlanetaFerengi;
        // angulo real del prediccion Betasoides para un dia especifico
        int posicionPlanetaBetasoides;
        // angulo real del prediccion Vulcanos para un dia especifico
        int posicionPlanetaVulcanos;
        double positionXFerengi;
        double positionYFerengi;
        double positionXVulcano;
        double positionYVulcano;
        double positionXBetasoides;
        double positionYBetasoides;

        for(int i = 1; i <= dias; i++) {
            posicionPlanetaFerengi = PlanetaEnum.FERENGI.getRealAngle(i);
            posicionPlanetaBetasoides = PlanetaEnum.BETASOIDES.getRealAngle(i);
            posicionPlanetaVulcanos = PlanetaEnum.VULCANOS.getRealAngle(i);
            // Si los angulos son iguales o con una diferencia de 180, entonces estan en la misma linea cruzando el sol, no necesitamos operaciones trigonometricas
            if (posicionPlanetaVulcanos % 180  == posicionPlanetaBetasoides % 180 && posicionPlanetaBetasoides % 180 == posicionPlanetaFerengi % 180) {
                if (lastWeather != ClimaEnum.SEQUIA) {
                    sequiaCount++;
                }
                lastWeather = ClimaEnum.SEQUIA;
            } else {
                // calculamos  el punto X,Y de cada planeta y lo llevamos a un espacio trigonometrico
                positionXFerengi = PlanetaEnum.FERENGI.positionX(posicionPlanetaFerengi);
                positionYFerengi = PlanetaEnum.FERENGI.positionY(posicionPlanetaFerengi);
                positionXVulcano = PlanetaEnum.VULCANOS.positionX(posicionPlanetaVulcanos);
                positionYVulcano = PlanetaEnum.VULCANOS.positionY(posicionPlanetaVulcanos);
                positionXBetasoides = PlanetaEnum.BETASOIDES.positionX(posicionPlanetaBetasoides);
                positionYBetasoides = PlanetaEnum.BETASOIDES.positionY(posicionPlanetaBetasoides);

                // caso 2, el sol adentro de un triangulo
                if (TriangleUtils.isInside(positionXFerengi, positionYFerengi, positionXBetasoides, positionYBetasoides, positionXVulcano, positionYVulcano, 0 ,0)) {
                    // perimetro es la suma de las distancias 3 entre 2 puntos
                    double distanceFB = Math.hypot(positionXFerengi - positionXBetasoides, positionYFerengi-positionYBetasoides);
                    double distanceFV = Math.hypot(positionXFerengi - positionXVulcano, positionYFerengi-positionYVulcano);
                    double distanceBV = Math.hypot(positionXBetasoides - positionXVulcano, positionYBetasoides-positionYVulcano);
                    double currentArea = distanceBV + distanceFB + distanceFV;
                    if (currentArea > maxArea) {
                        maxArea = currentArea;
                        maxDay = i;
                    }
                    if (lastWeather != ClimaEnum.LLUVIA) {
                        lluviaCount++;
                    }
                    lastWeather = ClimaEnum.LLUVIA;
                } else if ((positionYFerengi - positionYBetasoides) / (positionXFerengi - positionXBetasoides) ==
                        (positionYVulcano - positionYBetasoides) / (positionXVulcano - positionXBetasoides)) {
                    if (lastWeather != ClimaEnum.CONDICIONES_OPTIMAS) {
                        optimaCount++;
                    }
                    lastWeather = ClimaEnum.CONDICIONES_OPTIMAS;
                    continue;
                } else {
                    // en caso de no estar alineados de ninguna manera, decimos que es un dia normal
                    lastWeather = ClimaEnum.DIA_NORMAL;
                }
            }
            CondicionClimatica condicionClimatica = new CondicionClimatica(i, lastWeather);
            condicionClimaticaRepository.save(condicionClimatica);
        }
        Estadistica estadistica = new Estadistica();
        estadistica.setPeriodosSequia(sequiaCount);
        estadistica.setPeriodosLluvia(lluviaCount);
        estadistica.setPeriodosCondicionesOptimas(optimaCount);
        estadistica.setDiaMaximaLluvia(maxDay);
        estadisticaService.save(estadistica);
    }

}
