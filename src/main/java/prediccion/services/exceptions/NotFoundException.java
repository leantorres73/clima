package prediccion.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by leandro on 4/23/18.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class NotFoundException extends BusinessLogicException {

    private static final long serialVersionUID = 1L;

    public NotFoundException(String message) {
        super(message);
    }
}