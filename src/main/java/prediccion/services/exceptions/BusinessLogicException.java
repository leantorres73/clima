package prediccion.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by leandro on 4/23/18.
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class BusinessLogicException extends Exception {

    private static final long serialVersionUID = 1L;

    public BusinessLogicException(String message) {
        super(message);
    }
}
