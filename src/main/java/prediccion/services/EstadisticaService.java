package prediccion.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import prediccion.entities.ClimaEnum;
import prediccion.entities.CondicionClimatica;
import prediccion.entities.Estadistica;
import prediccion.entities.PlanetaEnum;
import prediccion.repositories.CondicionClimaticaRepository;
import prediccion.repositories.EstadisticaRepository;
import prediccion.services.exceptions.NotFoundException;
import prediccion.utils.TriangleUtils;

/**
 * Created by leandro on 4/23/18.
 */
@Service
@Transactional
public class EstadisticaService {

    private static final Logger log = LoggerFactory.getLogger(EstadisticaService.class);

    private EstadisticaRepository estadisticaRepository;

    public EstadisticaService(EstadisticaRepository estadisticaRepository) {
        this.estadisticaRepository = estadisticaRepository;
    }

    public Estadistica save(Estadistica estadistica) {
        log.debug("Guardando estadistica");
        return estadisticaRepository.save(estadistica);
    }

    public Estadistica findEstadistica() {
        log.debug("Get estadistica");
        return estadisticaRepository.findAll().get(0);
    }
}
