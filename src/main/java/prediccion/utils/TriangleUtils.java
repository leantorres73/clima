package prediccion.utils;

/**
 * Created by leandro on 7/22/18.
 */
public class TriangleUtils {

    /**
     * Checkeamos si un punto está dentro de un triangulo
     * */
    public static boolean isInside(double x1, double y1, double x2,
                            double y2, double x3, double y3, double x, double y) {
        double ABC = Math.abs (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2));
        double ABP = Math.abs (x1 * (y2 - y) + x2 * (y - y1) + x * (y1 - y2));
        double APC = Math.abs (x1 * (y - y3) + x * (y3 - y1) + x3 * (y1 - y));
        double PBC = Math.abs (x * (y2 - y3) + x2 * (y3 - y) + x3 * (y - y2));
        return ABP + APC + PBC == ABC;
    }

    /**
     * Calculamos el area de un triangulo
     * */
    public static double area(double x1, double y1, double x2, double y2, double x3, double y3) {
        double[] sides = new double[3];
        sides[0] = Math.sqrt(Math.pow(x1 - x2, 2)+Math.pow(y1 - y2, 2));
        sides[1] = Math.sqrt(Math.pow(x2 - x3, 2)+Math.pow(y2 - y3, 2));
        sides[2] = Math.sqrt(Math.pow(x3 - x1, 2)+Math.pow(y3 - y1, 2));
        double s = ( sides[0]+sides[1]+sides[2] )/2;
        return Math.sqrt(s*( s-sides[0] )*( s-sides[1] )*( s-sides[2] ));
    }
}
