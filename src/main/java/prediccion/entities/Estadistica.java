package prediccion.entities;

import javax.persistence.*;

/**
 * Created by leandro on 7/25/18.
 */
@Entity
public class Estadistica {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column
    Integer periodosSequia;

    @Column
    Integer periodosLluvia;

    @Column
    Integer periodosCondicionesOptimas;

    @Column
    Integer diaMaximaLluvia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPeriodosSequia() {
        return periodosSequia;
    }

    public void setPeriodosSequia(Integer periodosSequia) {
        this.periodosSequia = periodosSequia;
    }

    public Integer getPeriodosLluvia() {
        return periodosLluvia;
    }

    public void setPeriodosLluvia(Integer periodosLluvia) {
        this.periodosLluvia = periodosLluvia;
    }

    public Integer getPeriodosCondicionesOptimas() {
        return periodosCondicionesOptimas;
    }

    public void setPeriodosCondicionesOptimas(Integer periodosCondicionesOptimas) {
        this.periodosCondicionesOptimas = periodosCondicionesOptimas;
    }

    public Integer getDiaMaximaLluvia() {
        return diaMaximaLluvia;
    }

    public void setDiaMaximaLluvia(Integer diaMaximaLluvia) {
        this.diaMaximaLluvia = diaMaximaLluvia;
    }
}
