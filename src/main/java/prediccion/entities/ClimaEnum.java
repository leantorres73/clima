package prediccion.entities;

/**
 * Created by leandro on 7/22/18.
 */
public enum ClimaEnum {
    SEQUIA, LLUVIA, CONDICIONES_OPTIMAS, DIA_NORMAL
}
