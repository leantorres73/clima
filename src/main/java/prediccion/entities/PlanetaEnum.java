package prediccion.entities;

/**
 * Created by leandro on 7/25/18.
 */
public enum PlanetaEnum {
    FERENGI(500, -1),
    BETASOIDES(2000, -3),
    VULCANOS(1000, 5);

    private final Integer distancia;
    private final Integer velocidad;

    PlanetaEnum(Integer distancia, Integer velocidad) {
        this.distancia = distancia;
        this.velocidad = velocidad;
    }

    public Integer getDistancia() {
        return distancia;
    }

    public Integer getVelocidad() {
        return velocidad;
    }

    public double positionX(int angle) {
        return Math.cos(Math.toRadians(angle)) * distancia;
    }

    public double positionY(int angle) {
        return Math.sin(Math.toRadians(angle)) * distancia;
    }

    public int getRealAngle(int dia){
        return (360 + (dia * velocidad) % 360) % 360;
    }
}
