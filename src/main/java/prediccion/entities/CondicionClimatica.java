package prediccion.entities;

import javax.persistence.*;

/**
 * Created by leandro on 4/23/18.
 */
@Entity
public class CondicionClimatica {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column
    Integer dia;

    @Column
    ClimaEnum clima;

    public CondicionClimatica() {
    }

    public CondicionClimatica(Integer dia, ClimaEnum clima) {
        this.dia = dia;
        this.clima = clima;
    }

    public Integer getDia() {
        return dia;
    }

    public void setDia(Integer dia) {
        this.dia = dia;
    }

    public ClimaEnum getClima() {
        return clima;
    }

    public void setClima(ClimaEnum clima) {
        this.clima = clima;
    }
}
