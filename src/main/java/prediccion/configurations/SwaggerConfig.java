package prediccion.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Initialise docket, filtering the base package
     * and the paths of the endpoints to be documented
     *
     * @return Docket
     */
    @Bean
    public Docket swaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("prediccion"))
            .paths(regex("/api.*"))
            .build()
            .useDefaultResponseMessages(false)
            .apiInfo(metaInfo());
    }

    /**
     * Customise Swagger
     *
     * @return api customised infos
     */
    private ApiInfo metaInfo() {

        ApiInfo apiInfo = new ApiInfo(
                "Prediccion API",
                null,
                "1.0",
                null,
                new Contact("Leandro Torres", null,
                        "leandrotorres73@gmail.com"),
                null,
                null
        );

        return apiInfo;
    }
}
