package prediccion.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import prediccion.services.CondicionClimaticaService;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    CondicionClimaticaService condicionClimaticaService;

    Integer years;

    public ApplicationStartup(CondicionClimaticaService condicionClimaticaService,
                              @Value("${years}") Integer years) {
        this.condicionClimaticaService = condicionClimaticaService;
        this.years = years;
    }

    /**
     * This event is executed as late as conceivably possible to indicate that
     * the application is ready to service requests.
     */
    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        condicionClimaticaService.generateClimaticConditions(years);
    }
}