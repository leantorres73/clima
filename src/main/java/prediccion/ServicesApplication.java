package prediccion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Created by leandro on 4/23/18.
 */

@SpringBootApplication
public class ServicesApplication extends SpringBootServletInitializer {
    public static final String DEFAULT_URL = "/api";
    public static final String CLIMA_URL = DEFAULT_URL + "/clima";
    public static final String ESTADISTICA_URL = DEFAULT_URL + "/estadistica";

    public static void main(String[] args) {
        SpringApplication.run(ServicesApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ServicesApplication.class);
    }
}
