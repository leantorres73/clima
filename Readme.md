### Suposiciones
Como hay diversas cosas que no están especificadas hice las siguientes suposiciones:

- Se asume que un año es un año terrestre (365 días y no la vuelta al sol de uno de los 3 planetas).
- Se asume que cada planeta se calcula como un punto ya que no hay radio especificado (al igual que el sol).
- Se asume que los tres planetas arrancan del punto 0 de sus coordinadas Y.
- Se asume que al decir "cuántos períodos de ..." se busca un conjunto de 1 o más días seguidos. Tres días seguidos de 
sequía pertenecen al mismo período.
- Se asume que no interesa el dia 0 (dia actual) ya que están viviendo ese dia (pueden mirar al cielo), arrancamos a 
predecir del dia siguiente
- Se asume que un día que el sol no está dentro del triangulo formado por los tres planetas, es un día normal

### Notas:

Suelo crear interfaces para la capa de servicios y DTO's para todas las entidades con sus respectivos mappers.
En este caso lo mantuve simple por un tema de tiempo
También debería crear tests unitarios y de integración al menos

### Documentation. 

Se puede acceder a swagger para ver los endpoints disponibles:

https://prediccion-clima-211203.appspot.com/swagger-ui.html

## Endpoints:
Para visualizar todos los endpoints y su correspondiente documentación acceder a la página de swagger;
http://prediccion-clima-211203.appspot.com/swagger-ui.html

Hay dos endpoints disponibles:

Una peticion GET:
 https://prediccion-clima-211203.appspot.com/api/clima?dia= para obtener la condicion climatica para ese dia.
 
 
Una peticion GET:
 https://prediccion-clima-211203.appspot.com//api/estadistica, para obtener la estadistica de periodos de lluvia, etc.
 
